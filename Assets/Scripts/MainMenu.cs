using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class MainMenu : MonoBehaviour
{
    public Slider slider;
    public TextMeshProUGUI bestTimeText;
    public AudioSource audio;
    
    private string configPath;
    private string recordPath;
    void Start()
    {
        configPath = Application.persistentDataPath + "/config.cfg";
        recordPath = Application.persistentDataPath + "/records.txt";

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        float bestTime = 0f;
        TimeSpan ts;
        if(File.Exists(configPath))
        	using(FileStream file = new FileStream(configPath, FileMode.Open))
        	using(StreamReader stream = new StreamReader(file))
        		slider.value = float.Parse(stream.ReadLine());
        else
        	using(FileStream file = new FileStream(configPath, FileMode.Create))
        	using(StreamWriter stream = new StreamWriter(file))
        		stream.WriteLine(slider.value.ToString());

        if(File.Exists(recordPath))
        {
        	using(FileStream file = new FileStream(recordPath, FileMode.Open))
        	using(StreamReader stream = new StreamReader(file))
        		bestTime = float.Parse(stream.ReadLine());
        }
        else
        	using(FileStream file = new FileStream(recordPath, FileMode.Create))
        	using(StreamWriter stream = new StreamWriter(file))
        		stream.WriteLine(bestTime.ToString());

        ts = TimeSpan.FromSeconds(bestTime);       
        bestTimeText.text = "Best time:\n" + ts.ToString("mm':'ss':'ff") + "!";
    }

    public void OnValueChanged()
    {
        audio.volume = slider.value;
    }

    public void SaveVolume()
    {
        using(FileStream file = new FileStream(configPath, FileMode.Open))
        using(StreamWriter stream = new StreamWriter(file))
        	stream.WriteLine(slider.value.ToString());
    }
    public void StartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1, LoadSceneMode.Single);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}

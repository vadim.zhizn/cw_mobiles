using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public bool pressed = false;
    public float mouseSensitivity = 2f;
    public Transform playerBody;
    public Camera cam;

    private int fingerId;
    private float xRotation = 0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(pressed)
        {
            foreach(Touch touch in Input.touches)
            {
                if(touch.fingerId == fingerId)
                {
                    if(touch.phase == TouchPhase.Moved)
                    {
                        float mouseX = touch.deltaPosition.x * mouseSensitivity * Time.deltaTime;
                        float mouseY = touch.deltaPosition.y * mouseSensitivity * Time.deltaTime;
                    
                        xRotation -= mouseY;
                        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

                        cam.transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
                        playerBody.Rotate(Vector3.up * mouseX);
                    }
                }
            }
        }    
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if(eventData.pointerCurrentRaycast.gameObject == gameObject)
        {
            pressed = true;
            fingerId = eventData.pointerId;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        pressed = false;
    }
}

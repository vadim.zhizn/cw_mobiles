using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using System.IO;
using System;

public class GameController : MonoBehaviour
{
    public TextMeshProUGUI timer;
    public TextMeshProUGUI katanasCount;
    public int winCondition;
    public AudioSource themeAudio;
    public AudioSource winAudio;
    public GameObject winText;
    public GameObject dzenCow;

    private float elapsedTime = 0f;
    private int count;
    private bool stopped;
    private float oldBestTime;

    private string configPath;
    private string recordPath;

    void Start()
    {
        configPath = Application.persistentDataPath + "/config.cfg";
        recordPath = Application.persistentDataPath + "/records.txt";

        SetCountText();
        themeAudio.Play();
        winAudio.Stop();

        float volume;
        using(FileStream file = new FileStream(configPath, FileMode.Open))
        using(StreamReader stream = new StreamReader(file))
            volume = float.Parse(stream.ReadLine());

        using(FileStream file = new FileStream(recordPath, FileMode.Open))
        using(StreamReader stream = new StreamReader(file))
            oldBestTime = float.Parse(stream.ReadLine());

        themeAudio.volume = volume;
        winAudio.volume = volume;

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        elapsedTime += Time.deltaTime;

        if(!stopped)
        {   
            timer.text = $"Time: {TimeSpan.FromSeconds(elapsedTime).ToString("mm':'ss'.'ff")}";
        }
        else if(elapsedTime > 5)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1, LoadSceneMode.Single);
        }
    }

    void SetCountText()
    {
        katanasCount.text = $"Katanas: {count.ToString()} / {winCondition}";
        if(count >= winCondition)
        {
            themeAudio.Stop();
            winAudio.Play();
            stopped = true;
            winText.SetActive(true);
            dzenCow.SetActive(true);

            if(oldBestTime > elapsedTime || Mathf.Round(oldBestTime) == 0.0f)
            {  
                using(FileStream file = new FileStream(recordPath, FileMode.Open))
                using(StreamWriter stream = new StreamWriter(file))
                    stream.WriteLine(elapsedTime.ToString());
            }
            elapsedTime = 0;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count++;
            SetCountText();
        }
    }
}
